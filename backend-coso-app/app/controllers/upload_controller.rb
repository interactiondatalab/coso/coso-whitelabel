class UploadController < ApplicationController
  include ProgramPermission
  
  before_action :authenticate_user!
  before_action :set_program
  before_action :has_permission_admin, only: [:create, :destroy]

  def create
    new_teams = []
    new_participants = []

    for team in upload_params["teams"] do
      t = Team.find_or_create_by(name: team["name"], program: @program) do |new_team|
        new_teams.append(new_team.name)
      end

      for participant in team["participants"] do
        if participant[:email].empty?
          email = nil
        else
          email = participant[:email]
        end 
        
        Participant.find_or_create_by(name: participant[:name], email: email, team: t) do |new_participant|
          new_participants.append(new_participant.name)
        end
      end
    end
    render json: {"new_teams": new_teams, "new_participants": new_participants}, status: :ok
  end

  private

  def set_program
    if Program.exists?(params[:program_id])
      @program = Program.find(params[:program_id])
    else
      render json: {"error": "This program does not exist."}, status: :unprocessable_entity
    end
  end

  def upload_params
    params.require(:upload).permit(teams: [:name, participants: [:name, :email]])
  end

end
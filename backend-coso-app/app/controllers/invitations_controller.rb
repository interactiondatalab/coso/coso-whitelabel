class InvitationsController < Devise::InvitationsController
  include ProgramPermission

  before_action :authenticate_user!, only: [:create, :delete]
  before_action :set_program, only: [:create, :delete]
  before_action :has_permission_admin, only: [:create, :delete]

  def generate_url(url, params = {})
    uri = URI(url)
    uri.query = params.to_query
    uri.to_s
  end

  def edit
    if params[:invitation_token]
      redirect_to generate_url(ENV["FRONTEND_URL"] + "/invite", {invitation_token: params[:invitation_token]})
    end
  end

  def create
    user = User.invite!(options={email: invitation_params[:email], skip_invitation: true})

    if user.nil?
      render json: user.errors, status: :unprocessable_entity
    else
      is_granted = grant(user, invitation_params[:permission])

      if is_granted
        name = invitation_params[:email]
        unless invitation_params[:participant_id].nil?
          participant = Participant.find(invitation_params[:participant_id])

          unless participant.nil?
            name = participant.name
            participant.user = user
            participant.save
          end
        end

        if user.invitation_accepted?
          InvitationMailer.with(params: {"program_name": @program.name, "name": name, "email": user.email}).new_notification_mail.deliver_now
        else
          link = ENV["FRONTEND_URL"] + "/invite?invitation_token=" + user.raw_invitation_token

          InvitationMailer.with(params: {"program_name": @program.name, "name": name, "email": user.email, "link": link}).new_invitation_mail.deliver_now
        end

        render json: user, status: :created
      else
        render json: {"error": "This user has already this type of permission on this program."}, status: :unprocessable_entity
      end
    end
  end

  def update
    user = User.find_by_invitation_token(accept_invitation_params[:invitation_token], true)

    if user.nil?
      render json: {"error": "This user doesn't exist."}, status: :unprocessable_entity
    else
      User.accept_invitation!(invitation_token: accept_invitation_params[:invitation_token], password: accept_invitation_params[:password])
      render json: user, status: :ok
    end
  end

  def delete
    user = User.find_by(email: invitation_params[:email])

    if user.nil?
      render json: {"error": "This user does not exist."}, status: :unprocessable_entity
    else
      is_revoked = revoke(user, invitation_params[:permission])

      if is_revoked and user.roles.size == 1
        user.destroy
      end

      render status: :ok
    end
  end

  private

  def get_role_on_program(user, permission)
    user.roles.map do |role|
      if permission == role.name and role.resource_id == @program.id
        return role
      end
    end

    return nil
  end

  def grant(user, permission)
    unless permission == "god"
      role = get_role_on_program(user, permission)

      if role.nil?
        user.add_role invitation_params[:permission], @program
        return true
      end
    end

    return false
  end

  def revoke(user, permission)
    unless permission == "god"
      role = get_role_on_program(user, permission)

      unless role.nil?
        role.destroy
        return true
      end
    end

    return false
  end

  def set_program
    if Program.exists?(params[:program_id])
      @program = Program.find(params[:program_id])
    else
      render json: {"error": "This program does not exist."}, status: :unprocessable_entity
    end
  end

  def accept_invitation_params
    params.require(:user).permit(:invitation_token, :password)
  end

  def invitation_params
    params.require(:user).permit(:email, :permission, :participant_id)
  end
end

class InvitationMailer < Devise::Mailer
  default from: ENV['EMAIL']

  def new_invitation_mail
    @params = params[:params]

    mail(to: @params[:email], subject: "Welcome to CoSo")
  end

  def new_notification_mail
    @params = params[:params]

    mail(to: @params[:email], subject: "New invitation on CoSo")
  end

end

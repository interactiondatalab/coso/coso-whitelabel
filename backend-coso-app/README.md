# Instructions

## Add superuser
Inside the *coso-backend* container run:
- Open a console: 
```
rails c
```
- Create your user:
```
u = User.create(email: "email", password: "password")
u.add_role(:god)
```

import React, { useState, useEffect, useContext } from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate
} from "react-router-dom";

import Home from "./pages/Home";
import About from "./pages/About";
import { Login, Logout } from "./pages/Login";
import CreateProgram from "./pages/CreateProgram";
import Program from './pages/Program';
import Invitation from './pages/Invitation';
import TakeSurvey from './pages/TakeSurvey';
import Dataviz from './pages/Dataviz';
import Surveys from './pages/Surveys';
import ResponsiveContainer from "./components/ResponsiveContainer";
import Header from "./components/Header";
import { Context, cleanContext, isParticipant, isModerator, isAdmin, isGod } from './utils/Context';
import { Api, AxiosInterceptors } from './Api';

function App() {
  const context = useContext(Context);
  const [user, setUser] = useState({});
  const [firstLoad, setFirstLoad] = useState(true);
  const [programs, setPrograms] = useState({});
  const [currentProgram, setCurrentProgram] = useState("");
  const [currentParticipant, setCurrentParticipant] = useState({});
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    if(!firstLoad) {

      if(currentProgram.length === 2) {
        const program = currentProgram[0];
        const indexProgram = currentProgram[1];

        if(indexProgram < programs[program].length) {
          const permission = programs[program][indexProgram].permission;

          if(permission === 0) {
            const getParticipant = async() => {
              await Api.get('programs/' + programs[program][indexProgram].id + '/participant')
              .then((res) => {
                setCurrentParticipant(res.data);
                localStorage.setItem("CurrentParticipant", JSON.stringify(res.data));
              })
              .catch((error) => {
                console.log(error);
              })
            }

            getParticipant();
          }
        }

        localStorage.setItem("CurrentProgram", JSON.stringify(currentProgram));
      }
    }

    // console.log("currentProgram: " + currentProgram)
  }, [currentProgram])

  const loadPrograms = async(context) => {
    await Api.get('programs')
    .then((res) => {
      const listPrograms = {};

      res.data.sort((p1, p2) => p1.name.toLowerCase() > p2.name.toLowerCase() ? 1 : -1).map((p, i) => {
        let permissions = [];

        if(isParticipant(context, p.id)) {
          permissions.push(0);
        }
        if(isModerator(context, p.id)) {
          permissions.push(1);
        }
        if(isAdmin(context, p.id)) {
          permissions.push(2);
        }
        if(isGod(context)) {
          permissions.push(3);
        }


        if(permissions.length > 0) {
          listPrograms[p.name.replaceAll(" ", "_")] = [];

          permissions.map((permission) => {
            listPrograms[p.name.replaceAll(" ", "_")].push({ "id": p.id, "name": p.name, "description": p.description, "year": p.year, "permission": permission });
          })
        }
      })

      setPrograms(listPrograms);
      localStorage.setItem("Programs", JSON.stringify(listPrograms));
    })
    .catch((error) => {
      console.log(error);
    });
  }

  useEffect(() => {
    if(firstLoad) {
      const getUser = async() => {
        await Api.get('users/self')
        .then((res) => {
          setUser(res.data);
          localStorage.setItem("User", JSON.stringify(res.data));
        })
        .catch((error) => {
          cleanContext();
        });
      }

      if(localStorage.getItem("Authorization")) {
        getUser();

        const programs_data = JSON.parse(localStorage.getItem("Programs"));
        if(programs_data) {
          setPrograms(programs_data);
        } else {
          setPrograms({});
        }

        const current_program = JSON.parse(localStorage.getItem("CurrentProgram"));
        if(current_program) {
          setCurrentProgram(current_program);
        } else {
          setCurrentProgram([]);
        }
      }

      setFirstLoad(false);
    }
  }, [firstLoad]);

  return (
    <Context.Provider value={{firstLoad, user, setUser, programs, setPrograms, loadPrograms, currentProgram, setCurrentProgram, currentParticipant, setCurrentParticipant, isMobile, setIsMobile}}>
      <BrowserRouter>
        <AxiosInterceptors>
          <ResponsiveContainer>
            <Routes>
              <Route path="*" element={<Navigate to="/" replace />} />
              <Route path="/program/:program" element={<Program />} />
              <Route path="/program/create" element={<CreateProgram />} />
              <Route path="/program/:program/surveys" element={<Surveys />} />
              <Route path="/survey" element={<TakeSurvey />} />
              <Route path="/program/:program/dataviz" element={<Dataviz />} />
              <Route path="/" element=
                {<>
                  { user.email ? null : <Header /> }
                  <Home />
                </>} />
              <Route path="/about" element={<About />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/invite" element={<Invitation />} />
            </Routes>
          </ResponsiveContainer>
        </AxiosInterceptors>
      </BrowserRouter>
    </Context.Provider>
  )
}

export default App;

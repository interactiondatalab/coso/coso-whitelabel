import { useEffect, useState } from 'react';
import { useSearchParams, useNavigate } from "react-router-dom";
import {
  Segment,
  Container,
  Grid,
  Form
 } from 'semantic-ui-react';
import jwt_decode from "jwt-decode";

import { Api } from '../Api';
import Survey from '../components/Survey';

const TakeSurvey = () => {
  const history = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const [survey, setSurvey] = useState({});
  const [surveyFields, setSurveyFields] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingSurvey, setLoadingSurvey] = useState(true);
  const [params, setParams] = useState(null);
  const [displaySurvey, setDisplaySurvey] = useState(false);

  const [dataTeams, setDataTeams] = useState({});
  const [participants, setParticipants] = useState([]);
  const [teams, setTeams] = useState([]);
  const [selectParticipant, setSelectParticipant] = useState(null);
  const [selectTeam, setSelectTeam] = useState(null);
  const [disabled, setDisabled] = useState(true);
  const [respondantID, setRespondantID] = useState(-1);

  useEffect(() => {
    if(loading) {
      try {
        const object = jwt_decode(searchParams.get("part"));
        setParams(object);
      } catch(error) {
        console.log("Bad token");
      }
    }
  }, [loading])

  useEffect(() => {
    if(params) {
      if(!Object.keys(params).includes("participantId")) {
        Api.get('programs/' + params.programId + '/surveys/' + params.surveyId + "/participants")
        .then((res) => {
          setDataTeams(res.data);
          
          let newTeams = [];

          Object.entries(res.data).map(([key, value], j) => {
            newTeams.push({
              key: j+1,
              text: key,
              value: key
            })
          })

          setLoading(false);
          setTeams(newTeams);
        })
      } else {
        setRespondantID(params.participantId)
        setDisplaySurvey(true)
      }
    }
  }, [params])


  useEffect(() => {
    if(Object.keys(dataTeams).length > 0) {
      if(Object.keys(params).includes("teamName")) {
        setSelectTeam(params.teamName)
      }
    }
  }, [dataTeams])

  useEffect(() => {
    if(displaySurvey) {
      Api.get('programs/' + params.programId + '/surveys/' + params.surveyId)
     .then((res) => {
       res.data.survey_fields.sort((s1, s2) => s1.order > s2.order ? 1 : -1);
       setSurvey(res.data);
       setSurveyFields(res.data.survey_fields);
       setLoadingSurvey(false);
     })
     .catch((error) => {
      setLoadingSurvey(false);
     })
    }
  }, [displaySurvey])

  const sendSurvey = async(responses) => {
    await Promise.all(
      Object.entries(responses).map(([key, value], i) => {
        const survey_datum = {
          "participant_id": respondantID,
          "content": { "answer": value }
        }
        
        Api.post('programs/' + params.programId + '/surveys/' + params.surveyId + '/survey_fields/' + key + "/survey_data", {survey_datum})
        .catch((error) => {
          console.log(error);
        })
      })
    )

    history("/");
  }

  useEffect(() => {
    if(selectTeam !== null) {
      let newParticipants = [];
      let i = 0;

      dataTeams[selectTeam].sort((p1, p2) => {return p1.name > p2.name}).map((participant) => {
        newParticipants.push({
          key: i,
          text: participant.name,
          value: participant.id
        })

        i++;
      })

      setParticipants(newParticipants);
    }
  }, [selectTeam])

  const handleSelectTeam = (value) => {
    setSelectParticipant(null);
    setSelectTeam(value);
  }

  const handleSelectParticipant = (value) => {
    setSelectParticipant(value);
    setRespondantID(value);
    setDisabled(false);
  }


  return (
    <>
    { displaySurvey ?
      <>
      { loadingSurvey ?  
          null
        :
          <Grid stackable>
            <Grid.Column width={2} />
            <Grid.Column width={12}>
              <Segment basic>
                <Survey survey={survey} surveyFields={surveyFields} sendSurvey={sendSurvey} program_id={params.programId} participantId={respondantID} />
              </Segment>
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid>
      }
      </>
    :
      <Container fluid style={{ width: "50%", marginTop: "1em" }}>
        <Segment loading={loading}>
          <h4>To access the survey please select your team and your name in the list below:</h4>
          <Form>
            <Form.Dropdown search options={teams} label="Select a team" fluid selection value={selectTeam} onChange={(e, {value}) => handleSelectTeam(value)} />
            <Form.Dropdown options={participants} label="Select a participant" fluid selection value={selectParticipant} onChange={(e, {value}) => handleSelectParticipant(value)} />
            <Form.Button disabled={disabled} onClick={() => { setDisplaySurvey(true) }}>Begin</Form.Button>
          </Form>
        </Segment>
      </Container>
    }
    </>
  )

}

export default TakeSurvey;

import React, { useEffect, useState, useContext } from 'react';
import {
  Segment,
  Container,
  Header,
  Menu,
  Grid,
  Divider,
  Card,
  Image,
  Icon,
  Form,
  Button,
  Label,
  Input,
  Checkbox,
  Dropdown
 } from 'semantic-ui-react';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { LocalizationProvider, MobileDatePicker } from '@mui/x-date-pickers';
import { TextField } from '@mui/material';

import "../index.css";
import { Api } from '../Api';
import {
  FormIntro,
  FormInput
} from "./SurveyForm";

const nameCategory = { 
  "inputCheckbox": "Multiple choice", 
  "inputField": "Short answer", 
  "inputRadio": "Single choice", 
  "inputTextarea": "Paragraph", 
  "inputSlider": "Slider", 
  "inputDate": "Date", 
  "inputNames": "Interactions",
  "inputMatrix": "Matrix" ,
  "inputTopic": "Topic",
};

const categories = [
  {
    key: 0,
    text: nameCategory['inputCheckbox'],
    value: 'inputCheckbox',
  },
  {
    key: 1,
    text: nameCategory['inputDate'],
    value: 'inputDate',
  },
  {
    key: 2,
    text: nameCategory['inputField'],
    value: 'inputField',
  },
  {
    key: 3,
    text: nameCategory['inputTextarea'],
    value: 'inputTextarea',
  },
  {
    key: 4,
    text: nameCategory['inputNames'],
    value: 'inputNames',
  },
  {
    key: 5,
    text: nameCategory['inputSlider'],
    value: 'inputSlider',
  },
  {
    key: 6,
    text: nameCategory['inputRadio'],
    value: 'inputRadio',
  },
  {
    key: 7,
    text: nameCategory['inputMatrix'],
    value: 'inputMatrix',
  },
  {
    key: 8,
    text: nameCategory['inputTopic'],
    value: 'inputTopic',
  }
]

const FormSelect = ({ addSurveyField }) => {
  const [category, setCategory] = useState('');

  const add = () => {
    if(category !== "") {
      addSurveyField(category)
    }
  }

  return (
    <Grid verticalAlign="top" stackable columns={2}>
      <Grid.Row>
        <Grid.Column width={8}>
          <Segment attached padded>
            <Form>
              <Form.Dropdown
                label="Add new question"
                placeholder='Select Field'
                selection
                fluid
                options={categories}
                onChange={(e, {value}) => {setCategory(value)}}
              />
              <Form.Button onClick={() => {add()}}>Add</Form.Button>
            </Form>
          </Segment>
        </Grid.Column>
        <Grid.Column width={8} />
      </Grid.Row>
    </Grid>
  )
}

const Frame = ({ index, category, delForm, moveUp, moveDown }) => {
  return (
    <Segment attached="top" inverted style={{padding: 0}}>
      <Grid>
        <Grid.Column width={2} textAlign="left">
          <Button size="tiny" icon="arrow up" color="black" onClick={() => {moveUp(index)}} />
          <Button size="tiny" icon="arrow down" color="black" onClick={() => {moveDown(index)}} />
        </Grid.Column>
        <Grid.Column width={6} textAlign="left" verticalAlign="middle">
          <p>Question {index+1} - {nameCategory[category]}</p>
        </Grid.Column>
        <Grid.Column width={8} textAlign="right">
          <Button size="tiny" icon="close" color="black" onClick={() => {delForm(index)}} />
        </Grid.Column>
      </Grid>
    </Segment>
  )
}

const FormControl = ({ Display, Input, surveyField, setContent, refresh, index, delForm, moveUp, moveDown, surveyFields, program_id, teams, can_required}) => {
  const [title, setTitle] = useState(surveyField.title);
  const [subtitle, setSubtitle] = useState(surveyField.subtitle);
  const [dependency, setDependency] = useState(surveyField.dependency);
  const [required, setRequired] = useState(surveyField.required);

  const setNewDependency = (newDependency) => {
    setDependency(newDependency);
  }

  useEffect(() => {
    setTitle(surveyField.title);
    setSubtitle(surveyField.subtitle);
    setDependency(surveyField.dependency);
  }, [refresh])

  useEffect(() => {
    updateContent();
  }, [title, subtitle, dependency, required]);

  const updateContent = () => {
    let newSurveyField = {...surveyField};
    
    newSurveyField.title = title;
    newSurveyField.subtitle = subtitle;
    newSurveyField.dependency = dependency;
    newSurveyField.required = required;

    setContent(newSurveyField);
  }

  return (
    <Grid verticalAlign="top" stackable columns={2}>
      <Grid.Row>
        <Grid.Column width={8}>
          <Frame index={index} category={surveyField.category} delForm={delForm} moveUp={moveUp} moveDown={moveDown} />
          <Segment attached padded>
            <Form>
              <Form.Input
                label='Question title'
                placeholder='A great question title.'
                value={title}
                required
                onChange={(e, {value}) => setTitle(value)}
              />
              <Form.TextArea
                label='Description'
                value={subtitle}
                placeholder='A great question description.'
                onChange={(e, {value}) => setSubtitle(value)}
              />
              <Divider horizontal section>Content</Divider>
              <Display surveyField={surveyField} setContent={setContent} refresh={refresh} surveyFields={surveyFields} program_id={program_id} />
              <Divider horizontal fitted style={{paddingBottom: "2em"}}>Properties</Divider>
              { can_required ?
                  <>
                    <Form.Field>
                      <Checkbox value={required} checked={required === 0 ? false : true} onClick={() => setRequired(required === 0 ? 1 : 0)} label="Required" />
                    </Form.Field>
                    <DependencyControl surveyField={surveyField} index={index} surveyFields={surveyFields} setNewDependency={setNewDependency} refresh={refresh} />
                  </>
                :
                  <DependencyControl surveyField={surveyField} index={index} surveyFields={surveyFields} setNewDependency={setNewDependency} refresh={refresh} />
              }
            </Form>
          </Segment>
        </Grid.Column>
        <Grid.Column width={8}>
          <FormInput Input={Input} surveyField={surveyField} displayBorder={true} program_id={program_id} participantId={-1} teams={teams} />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}

const FormIntroControl = ({ program_id, refresh, survey, setContent }) => {
  const [name, setName] = useState(survey.name);
  const [description, setDescription] = useState(survey.description);
  const [teams, setTeams] = useState(survey.team_id);
  const [loading, setLoading] = useState(true);
  const [options, setOptions] = useState([]);

  useEffect(() => {
    if(loading) {
      Api.get('programs/' + program_id + '/teams')
      .then((res) => {
        let newOptions = [];
        let i=0;
        res.data.forEach(team => {
          newOptions.push({
            key: i,
            text: team.name,
            value: team.id
          });

          i++;
        })
        setLoading(false);
        setOptions(newOptions);
      })
      .catch(error => {
        console.log(error);
        setLoading(false);
      })
    }
  }, [loading])

  useEffect(() => {
    setName(survey.name);
    setDescription(survey.description);
    setTeams(survey.team_id);
  }, [refresh])

  useEffect(() => {
    let newSurvey = {...survey};
    newSurvey.name = name;
    newSurvey.description = description
    newSurvey.team_id = teams;

    setContent(newSurvey);

  }, [name, description, teams]);

  return (
    <Grid verticalAlign="top" stackable columns={2}>
      <Grid.Row>
        <Grid.Column width={8}>
          <Segment attached padded loading={loading}>
            <Form>
              <Form.Dropdown label='Use the navigator below if you want to restrict the surveys to a subset of teams. If LEFT BLANK, the survey will be sent to all participants.' fluid multiple selection options={options} onChange={(e, {value}) => setTeams(value)} value={teams} />
              <Form.Input
                label='Survey title'
                value={name}
                placeholder='A great new title.'
                required
                onChange={(e, {value}) => {setName(value)}}
              />
              <Form.TextArea
                label='Survey description'
                value={description}
                required
                placeholder='Some interesting description.'
                onChange={(e, {value}) => setDescription(value)}
              />
            </Form>
          </Segment>
        </Grid.Column>
        <Grid.Column width={8}>
          <FormIntro survey={survey} />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}

const DependencyControl = ({surveyField, index, surveyFields, setNewDependency, refresh}) => {
  const [questions, setQuestions] = useState([]);
  const [surveyFieldIndex, setSurveyFieldIndex] = useState(surveyField.dependency.survey_field_id);
  const [dependencyValue, setDependencyValue] = useState(surveyField.dependency.value);
  const [typeDependency, setTypeDependency] = useState(surveyField.dependency.value === "" ? 1 : 2);

  useEffect(() => {
    setNewDependency({ "survey_field_id": surveyFieldIndex, "value": dependencyValue });
  }, [surveyFieldIndex, typeDependency, dependencyValue])

  useEffect(() => {
    setSurveyFieldIndex(surveyField.dependency.survey_field_id);
    setDependencyValue(surveyField.dependency.value);
    setTypeDependency(surveyField.dependency.value === "" ? 1 : 2);

    let surveyQuestions = [];
    for(let i=0; i<surveyFields.length; i++) {
      if(index > i && surveyFields[i].title && surveyFields[i].dependency.survey_field_id === "") {
        let text = "Question " + (i+1) +": " + surveyFields[i].title;
        surveyQuestions.push({
          key: i,
          text: text,
          value: i
        })
      }
    }

    setQuestions(surveyQuestions);
  }, [refresh])

  return (
    <>
    { questions.length > 0 ?
        <Form.Dropdown
          placeholder='Select a question'
          fluid
          clearable
          label='Dependency'
          selection
          closeOnBlur={false}
          options={questions}
          onChange={(e, {value}) => setSurveyFieldIndex(value)}
          value={surveyFieldIndex}
        />
      :
        null
    }
    { surveyFieldIndex === '' ?
        null
      :
          <Form.Field>
            <Checkbox radio label="Scan across answer(s) of the dependency question." checked={typeDependency === 1} onChange={() => {setDependencyValue(''); setTypeDependency(1)}} />
          <Form.Field>
          </Form.Field>
            <Checkbox radio label="If value of answer(s) equals to ..." checked={typeDependency === 2} onChange={() => setTypeDependency(2)} />
          </Form.Field>
    }
    { typeDependency === 2 ?
        <Form.Input placeholder="Enter a value of the dependency question that will trigger this question." value={dependencyValue} fluid onChange={(e, {name, value}) => setDependencyValue(value)}></Form.Input>
      :
        null
    }
    </>
  )
}

const DisplayInputField = ({ surveyField, setContent, refresh, program_id }) => {
  const [placeholder, setPlaceholder] = useState(surveyField.content.placeholder);
  
  useEffect(() => {
    setPlaceholder(surveyField.content.placeholder);
  }, [refresh])

  useEffect(() => {
    let newSurveyField = {...surveyField};
    
    newSurveyField.content.placeholder = placeholder;

    setContent(newSurveyField);
  }, [placeholder]);

  return (
    <>
      <Form.Input
        label='Placeholder'
        value={placeholder}
        placeholder='A great placeholder.'
        onChange={(e, {value}) => setPlaceholder(value)}
      />
    </>
  )
}

const DisplayInputTextarea = ({ surveyField, setContent, refresh, program_id }) => {
  const [placeholder, setPlaceholder] = useState(surveyField.content.placeholder);
  
  useEffect(() => {
    setPlaceholder(surveyField.content.placeholder);
  }, [refresh])

  useEffect(() => {
    let newSurveyField = {...surveyField};
    
    newSurveyField.content.placeholder = placeholder;

    setContent(newSurveyField);
  }, [placeholder]);

  return (
    <>
      <Form.Input
        label='Placeholder'
        value={placeholder}
        placeholder='A great placeholder.'
        onChange={(e, {value}) => setPlaceholder(value)}
      />
    </>
  )
}

const DisplayInputNames = ({ surveyField, setContent, refresh, program_id }) => {
  const [onlyInnerTeam, setOnlyInnerTeam] = useState(0); 
  const [teams, setTeams] = useState(surveyField.content.teams);
  const [loading, setLoading] = useState(true);
  const [options, setOptions] = useState([]);

  useEffect(() => {
    if(loading) {
      Api.get('programs/' + program_id + '/teams')
      .then((res) => {
        let newOptions = [];
        let i=0;
        res.data.forEach(team => {
          newOptions.push({
            key: i,
            text: team.name,
            value: team.id
          });

          i++;
        })

        setOptions(newOptions);
      })
      .catch(error => {
        console.log(error);
      })

      setLoading(false);
    }
  }, [loading])

  useEffect(() => {
    if(surveyField.content.teams.length === 1 && surveyField.content.teams[0] === -1) {
      setOnlyInnerTeam(1);
    }

    setTeams(surveyField.content.teams);
    
  }, [refresh])

  useEffect(() => {
    let newSurveyField = {...surveyField};
    
    if(surveyField.content.teams.length === 1 && surveyField.content.teams[0] === -1) {
      if(onlyInnerTeam === 0) {
        newSurveyField.content.teams = [];
      }
    } else {
      if(onlyInnerTeam === 1) {
        newSurveyField.content.teams = [-1];
      } else {
        newSurveyField.content.teams = teams;
      }
    }

    setContent(newSurveyField);
  }, [onlyInnerTeam, teams]);

  return (
    <>
      <Checkbox value={onlyInnerTeam} checked={onlyInnerTeam === 0 ? false : true} onClick={() => setOnlyInnerTeam(onlyInnerTeam === 0 ? 1 : 0)} label="Only participant's team" />
      <Form.Dropdown disabled={onlyInnerTeam === 0 ? false : true} label="Use the navigator below if you want to restrict the question to a subset of teams. If left blank, all the teams will be displayed." placeholder='' fluid multiple selection options={options} onChange={(e, {value}) => setTeams(value)} value={teams} />
    </>
  )
}

const DisplayInputCheckbox = ({ surveyField, setContent, refresh, program_id }) => {
  const [labels, setLabels] = useState(surveyField.content.labels);
  
  useEffect(() => {
    setLabels(surveyField.content.labels);
  }, [refresh])

  useEffect(() => {
    let newSurveyField = {...surveyField};
    
    newSurveyField.content.labels = labels;

    setContent(newSurveyField);
  }, [labels]);

  const handleLabel = (index, value) => {
    let newLabels = [...labels];
    newLabels[index] = value;
    setLabels(newLabels);
  }

  const addLabel = () => {
    let newLabels = [...labels];
    newLabels.push('');
    setLabels(newLabels);
  }

  const delLabel = (index) => {
    let newLabels = [...labels];
    newLabels.splice(index, 1);
    setLabels(newLabels);
  }

  return (
    <>
      { labels.map((label, i) => {
          return (
            <Form.Group inline key={i}>
              <Form.Input label="Label" value={label} placeholder="A label" onChange={(e, {value}) => {handleLabel(i, value)}} />
              <Form.Button icon="trash" onClick={() => {delLabel(i)}} />
            </Form.Group>
          )
        })
      }
      <Form.Button icon="plus" onClick={addLabel} />
    </>
  )
}

const DisplayInputRadio = ({ surveyField, setContent, refresh, program_id }) => {
  const [labels, setLabels] = useState(surveyField.content.labels);
  
  useEffect(() => {
    setLabels(surveyField.content.labels);
  }, [refresh])

  useEffect(() => {
    let newSurveyField = {...surveyField};
    
    newSurveyField.content.labels = labels;

    setContent(newSurveyField);
  }, [labels]);

  const handleLabel = (index, value) => {
    let newLabels = [...labels];
    newLabels[index] = value;
    setLabels(newLabels);
  }

  const addLabel = () => {
    let newLabels = [...labels];
    newLabels.push('');
    setLabels(newLabels);
  }

  const delLabel = (index) => {
    let newLabels = [...labels];
    newLabels.splice(index, 1);
    setLabels(newLabels);
  }

  return (
    <>
      { labels.map((label, i) => {
          return (
            <Form.Group inline key={i}>
              <Form.Input label="Label" value={label} placeholder="A label" onChange={(e, {value}) => {handleLabel(i, value)}} />
              <Form.Button icon="trash" onClick={() => {delLabel(i)}} />
            </Form.Group>
          )
        })
      }
      <Form.Button icon="plus" onClick={addLabel} />
    </>
  )
}

const DisplayInputSlider = ({ surveyField, setContent, refresh, program_id }) => {
  const [min, setMin] = useState(surveyField.content.min);
  const [textMin, setTextMin] = useState(surveyField.content.textMin);
  const [max, setMax] = useState(surveyField.content.max);
  const [textMax, setTextMax] = useState(surveyField.content.textMax);
  const [step, setStep] = useState(surveyField.content.step);
  const [defaultValue, setDefaultValue] = useState(surveyField.content.default);
  
  useEffect(() => {
    setMin(surveyField.content.min);
    setMax(surveyField.content.max);
    setStep(surveyField.content.step);
    setDefaultValue(surveyField.content.default);
  }, [refresh])

  useEffect(() => {
    let newSurveyField = {...surveyField};
    newSurveyField.content.min = min;
    newSurveyField.content.textMin = textMin;
    newSurveyField.content.max = max;
    newSurveyField.content.textMax = textMax;
    newSurveyField.content.step = step;
    newSurveyField.content.default = defaultValue;

    setContent(newSurveyField);
  }, [min, textMin, max, textMax, step, defaultValue]);

  const isInt = (value) => {
    const x = parseFloat(value);
    return !isNaN(value) && (x | 0) === x;
  }

  const checkValue = (value) => {
    if(isInt(value)) {
      return parseInt(value);
    } else {
      return "";
    }
  }

  return (
    <>
      <Form.Input
        label='Min'
        value={min}
        placeholder=''
        onChange={(e, {value}) => setMin(checkValue(value))}
      />
      <Form.Input
        label='Text min'
        value={textMin}
        placeholder=''
        onChange={(e, {value}) => setTextMin(value)}
      />
      <Form.Input
        label='Max'
        value={max}
        placeholder=''
        onChange={(e, {value}) => setMax(checkValue(value))}
      />
      <Form.Input
        label='Text max'
        value={textMax}
        placeholder=''
        onChange={(e, {value}) => setTextMax(value)}
      />
      <Form.Input
        label='Default'
        value={defaultValue}
        placeholder=''
        onChange={(e, {value}) => setDefaultValue(checkValue(value))}
      />
      <Form.Input
        label='Step'
        value={step}
        placeholder=''
        onChange={(e, {value}) => setStep(checkValue(value))}
      />
    </>
  )
}

const DisplayInputDate = ({ surveyField, setContent, refresh, program_id }) => {
  const [maxDate, setMaxDate] = useState(surveyField.content.maxDate);
  const [minDate, setMinDate] = useState(surveyField.content.minDate);
  const [typeDate, setTypeDate] = useState(surveyField.content.typeDate);
  const [viewDate, setViewDate] = useState([]);
  const [formatDate, setFormatDate] = useState("yyyy-MM-DD");
  const [loading, setLoading] = useState(true);

  const options = [
    { key: 0, text: "Full date", value: 0 },
    { key: 1, text: "Months and days", value: 1 },
    { key: 2, text: "Only days", value: 2 },
    { key: 3, text: "Only months", value: 3 },
    { key: 4, text: "Only years", value: 4 }
  ];

  useEffect(() => {
    setMaxDate(surveyField.content.maxDate);
    setMinDate(surveyField.content.minDate);
  }, [refresh])

  const updateContent = () => {
    setLoading(false);
    let newSurveyField = {...surveyField};
    newSurveyField.content.maxDate = maxDate;
    newSurveyField.content.minDate = minDate;
    newSurveyField.content.typeDate = typeDate;

    setContent(newSurveyField);
  }

  useEffect(() => {
    switch(typeDate) {
      case 0:
        setViewDate(["year", "month", "day"]);
        setFormatDate("yyyy-MM-DD");
        break;
      case 1:
        setViewDate(["month", "day"]);
        setFormatDate("MM-DD");
        break;
      case 2:
        setViewDate(["day"]);
        setFormatDate("DD");
        break;
      case 3:
        setViewDate(["month"]);
        setFormatDate("MM");
        break;
      case 4:
        setViewDate(["year"]);
        setFormatDate("yyyy");
        break;
    }

    if(!loading) {
      setMaxDate(null);
      setMinDate(null);
    }

    updateContent();
  }, [typeDate])

  useEffect(() => {
    updateContent();
  }, [maxDate, minDate]);

  return (
    <>
      <Dropdown fluid selection options={options} value={typeDate} onChange={(e, {value}) => setTypeDate(value)} />
      <LocalizationProvider dateAdapter={AdapterMoment}>
        <Segment basic style={{paddingBottom: "0em"}}>
          <MobileDatePicker
            label="Start date"
            views={viewDate}
            value={minDate || null}
            inputFormat={formatDate}
            onChange={(value) => setMinDate(value)}
            renderInput={(params) => <TextField {...params} />}
            componentsProps={{
              actionBar: {
                actions: ['accept', 'clear'],
              },
            }}
          />
        </Segment>
        <Segment basic>
          <MobileDatePicker
            label="End date"
            views={viewDate}
            value={maxDate || null}
            inputFormat={formatDate}
            minDate={minDate}
            onChange={(value) => setMaxDate(value)}
            renderInput={(params) => <TextField {...params} />}
            componentsProps={{
              actionBar: {
                actions: ['accept', 'clear'],
              },
            }}
          />
        </Segment>
      </LocalizationProvider>
    </>
  )
}


const DisplayInputMatrix = ({ surveyField, setContent, refresh, program_id }) => {
  const [category, setCategory] = useState(surveyField.content.category);
  const [elementDisplay, setElementDisplay] = useState("");
  const [listDisplay, setListDisplay] = useState([]);
  const [listSurveyField, setListSurveyField] = useState([]);

  const createSurveyField = (category, surveyField = null) => {
    let newSurveyField = {
      "category": category,
      "subtitle": "",
      "content": {}
    };

    if(surveyField) {
      newSurveyField.subtitle = surveyField.subtitle;
      newSurveyField.required = surveyField.required ? 1 : 0;
      newSurveyField.content = surveyField.content;

      if(category === "inputDate") {
        if(newSurveyField.content.maxDate) {
          newSurveyField.content.maxDate = new Date(newSurveyField.content.maxDate);
        }
        if(newSurveyField.content.minDate) {
          newSurveyField.content.minDate = new Date(newSurveyField.content.minDate);
        }
      }
    } else {
      switch(category) {
        case "inputField":
          newSurveyField["content"]["placeholder"] = "";
          break;
        case "inputCheckbox":
          newSurveyField["content"]["labels"] = [];
          break;
        case "inputRadio":
          newSurveyField["content"]["labels"] = [];
          break;
        case "inputTextarea":
          newSurveyField["content"]["placeholder"] = "";
          break;
        case "inputNames":
          newSurveyField["content"]["teams"] = [];
          break;
        case "inputSlider":
          newSurveyField["content"]["min"] = 1;
          newSurveyField["content"]["textMin"] = "";
          newSurveyField["content"]["max"] = 5;
          newSurveyField["content"]["textMax"] = "";
          newSurveyField["content"]["step"] = 1;
          break;
        case "inputDate":
          newSurveyField["content"]["maxDate"] = null;
          newSurveyField["content"]["minDate"] = null;
          newSurveyField["content"]["typeDate"] = 0;
          break;
        case "inputMatrix":
          break;
      }
    }

    let newDisplay;
    switch(category) {
      case "inputField":
        newDisplay = DisplayInputField;
        break;
      case "inputCheckbox":
        newDisplay = DisplayInputCheckbox;
        break;
      case "inputRadio":
        newDisplay = DisplayInputRadio;
        break;
      case "inputTextarea":
        newDisplay = DisplayInputTextarea;
        break;
      case "inputNames":
        newDisplay = DisplayInputNames;
        break;
      case "inputSlider":
        newDisplay = DisplayInputSlider;
        break;
      case "inputDate":
        newDisplay = DisplayInputDate;
        break;
    }

    return [newSurveyField, newDisplay];
  }

  useEffect(() => {
    let newListDisplay = [];
    let newListSurveyField = [];
        
    if(category) {
      if(surveyField.content.contents.length > 0) {
        surveyField.content.contents.forEach(content => {
          const [newSurveyField, newDisplay] = createSurveyField(category);

          newListDisplay.push(newDisplay);
          newListSurveyField.push(content);  
        })
      } else {
        const [newSurveyField, newDisplay] = createSurveyField(category);

        newListDisplay.push(newDisplay);
        newListSurveyField.push(newSurveyField);
      }
    }

    setListDisplay(newListDisplay);
    setListSurveyField(newListSurveyField);
    
  }, [category])

  useEffect(() => {
    let newElementDisplay = [];

    if(listSurveyField.length > 0) {
      
      newElementDisplay.push(
        <Divider horizontal fitted style={{paddingBottom: "2em"}}>Matrix content</Divider>
      )

      if(listDisplay === 0) {
        let newListDisplay = []
        for(let i=0; i<listSurveyField.length; i++) {
          const [newSurveyField, newDisplay] = createSurveyField(category);
          newListDisplay.push(newDisplay)
        }
        
      }

      for(let i=0; i<listSurveyField.length; i++) {
        let Display = listDisplay[i];
        
        newElementDisplay.push(
          <Form.Input
            key={"subtitle_" + i}
            label='Subtitle'
            value={listSurveyField[i].subtitle}
            placeholder='A subtitle.'
            onChange={(e, {value}) => setSubtitle(i, value)}
          />
        )

        newElementDisplay.push(
          <Display key={"display_" + i} surveyField={listSurveyField[i]} setContent={(content) => {setContentMatrix(i, content)}} refresh={refresh} />
        )
        
        if(listSurveyField.length > 1) {
          newElementDisplay.push(
            <Button key={"button_" + i} className='right floated' icon="delete" onClick={() => deleteContent(i)} label="Remove content" />
          )
        }
        
        newElementDisplay.push(
          <div key={"div_" + i} className="ui divider"></div>
        )
      }
      
      newElementDisplay.push(
        <Button style={{marginTop: "1em"}} icon="plus" onClick={() => addContent()} label="Add content" />
      )
    }
    
    setElementDisplay(newElementDisplay);

    let newSurveyField = {...surveyField};
    newSurveyField.content.category = category;
    newSurveyField.content.contents = listSurveyField;
    setContent(newSurveyField);

  }, [listSurveyField])

  const setContentMatrix = (index, newContent) => {
    let newListSurveyField = [...listSurveyField];
    newListSurveyField[index] = newContent;
    setListSurveyField(newListSurveyField);
  }

  const setSubtitle = (index, value) => {
    let newListSurveyField = [...listSurveyField];
    newListSurveyField[index].subtitle = value;
    setListSurveyField(newListSurveyField);
  }

  const addContent = () => {
    const [newSurveyField, newDisplay] = createSurveyField(category);
    
    setListDisplay([...listDisplay, newDisplay]);
    setListSurveyField([...listSurveyField, newSurveyField]);
  }

  const deleteContent = (index) => {
    let newListDisplay = [...listDisplay];
    newListDisplay.splice(index, 1);
    setListDisplay(newListDisplay)

    let newListSurveyField = [...listSurveyField];
    newListSurveyField.splice(index, 1)
    setListSurveyField(newListSurveyField);
  }

  return (
    <>
      <Form.Dropdown
        label="Choose question type"
        placeholder='Select Field'
        selection
        fluid
        options={categories.filter(c => { return c.value !== "inputMatrix" && c.value !== "inputNames" })}
        value={category}
        onChange={(e, {value}) => {setCategory(value)}}
      />
      {elementDisplay}
    </>
  )
}

const DisplayInputTopic = ({ surveyField, setContent, refresh, program_id }) => {
  const [paragraphs, setParagraphs] = useState(surveyField.content.paragraphs);
  
  useEffect(() => {
    setParagraphs(surveyField.content.paragraphs);
  }, [refresh])

  useEffect(() => {
    let newSurveyField = {...surveyField};
    
    newSurveyField.content.paragraphs = paragraphs;

    setContent(newSurveyField);
  }, [paragraphs]);

  const handleParagraph = (index, value) => {
    let newParagraphs = [...paragraphs];
    newParagraphs[index].text = value;
    setParagraphs(newParagraphs);
  }

  const addParagraph = () => {
    let newParagraphs = [...paragraphs];
    newParagraphs.push({
      text: "",
      italic: false,
      bold: false
    });
    setParagraphs(newParagraphs);
  }

  const delParagraph = (index) => {
    let newParagraphs = [...paragraphs];
    newParagraphs.splice(index, 1);
    setParagraphs(newParagraphs);
  }

  const setItalic = (index) => {
    let newParagraphs = [...paragraphs];
    newParagraphs[index].italic = !paragraphs[index].italic;
    setParagraphs(newParagraphs);
  }

  const setBold = (index) => {
    let newParagraphs = [...paragraphs];
    newParagraphs[index].bold = !paragraphs[index].bold;
    setParagraphs(newParagraphs);
  }

  return (
    <>
      { paragraphs.map((paragraph, i) => {
          return (
            <>
              <Form.TextArea key={"text_" + i} label="Paragraph" value={paragraph.text} placeholder="Some text" onChange={(e, {value}) => {handleParagraph(i, value)}} />
              <Form.Field key={"text_formatting_" + i}>
                <Form.Checkbox checked={paragraph.italic} onClick={() => setItalic(i)} label="Italic" />
                <Form.Checkbox checked={paragraph.bold} onClick={() => setBold(i)} label="Bold" />
              </Form.Field>
              <Form.Button key={"button_" + i} icon="trash" onClick={() => {delParagraph(i)}} />
            </>
          )
        })
      }
      <Button icon="plus" onClick={addParagraph} />
    </>         
  )
}

export { 
  FormSelect,
  FormControl, 
  FormIntroControl, 
  DisplayInputField, 
  DisplayInputCheckbox, 
  DisplayInputRadio, 
  DisplayInputTextarea, 
  DisplayInputNames, 
  DisplayInputSlider, 
  DisplayInputDate, 
  DisplayInputMatrix,
  DisplayInputTopic
}

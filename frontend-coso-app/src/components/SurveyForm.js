import React, { useEffect, useState, useContext, useRef } from 'react';
import {
  Segment,
  Header,
  Grid,
  Form,
  Button,
  Checkbox
 } from 'semantic-ui-react';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { LocalizationProvider, MobileDatePicker } from '@mui/x-date-pickers';
import { TextField, Slider } from '@mui/material';

import "../index.css";
import { Api } from '../Api';

const FormIntro = (props) => {
  return (
    <Segment attached padded >
      <Header as="h2" className="display-linebreak">
        { props.survey.name }
      </Header>
      <p className="display-linebreak">{ props.survey.description }</p>
    </Segment>
  )
}

const FormWrapDependency = (props) => {
  const [responses, setResponses] = useState({});
  const [r, setR] = useState([]);
  const [forms, setForms] = useState([]);

  useEffect(() => {
    let newResponses = {};

    if(typeof props.responseDependency === 'string') {
      newResponses[props.responseDependency] = "";
    } else if(Array.isArray(props.responseDependency)) {
      props.responseDependency.forEach(value => {
        newResponses[value] = "";
      })
    } else if(typeof props.responseDependency === 'object') {
      Object.entries(props.responseDependency).map(([key, value], i) => {
        if(value === true || value === 1) {
          newResponses[key] = "";
        } else {
          newResponses[value] = "";
        }
      })
    }

    const newForms = Object.entries(newResponses).map(([key, value], i) => {
      let newSurveyField = {...props.surveyField};
      newSurveyField.id = key;
      newSurveyField.title = key;
      newSurveyField.subtitle = "";

      let response = '';
      if(props.response !== '' && key in props.response) {
        response = props.response[key];
      }

      let Input = null;
      switch(props.surveyField.category) {
        case "inputField": Input = FormInputField; break;
        case "inputCheckbox": Input = FormInputCheckbox; break;
        case "inputRadio": Input = FormInputRadio; break;
        case "inputTextarea": Input = FormInputTextarea; break;
        case "inputNames": Input = FormInputNames; break;
        case "inputSlider": Input = FormInputSlider; break;
        case "inputDate": Input = FormInputDate; break;
        case "inputMatrix": Input = FormInputMatrix; break;
        case "inputTopic": Input = FormInputTopic; break;
      }
      return <FormInput Input={Input} surveyField={newSurveyField} displayBorder={false} writeResponse={writeResponse} response={response} program_id={props.program_id} participantId={props.participantId} teams={newSurveyField.content.teams} />
    })

    setForms(newForms);
  }, [props])

  useEffect(() => {
    if(props.writeResponse !== null) {
      props.writeResponse(props.surveyField.id, responses);
    }
  }, [responses])

  const writeResponse = (index, response) => {
    setResponses((oldState) => ({...oldState, [index]: response }));
  }

  return (
    <Segment attached padded>
      <Header className="display-linebreak">
        { props.surveyField.title }
      </Header>
      <p className="display-linebreak">{ props.surveyField.subtitle }</p>
      {forms}
    </Segment>
  )
}

const FormInput = ({ Input, displayBorder, surveyField, response, writeResponse, program_id, participantId, teams }) => {
  return (
    <Segment basic={!displayBorder} attached={displayBorder} padded>
      <Header as="h2" className="display-linebreak">
        { surveyField.title }
      </Header>
      <p className="display-linebreak">{ surveyField.subtitle }</p>
      <Input surveyField={surveyField} response={response} writeResponse={writeResponse} program_id={program_id} participantId={participantId} teams={teams} />
    </Segment>
  )
}

const FormInputField = (props) => {
  const [response, setResponse] = useState('');
  
  useEffect(() => {
    if(props.response !== undefined) {
      setResponse(props.response);
    }
  }, [props])

  useEffect(() => {
    if(props.writeResponse !== undefined) {
      props.writeResponse(props.surveyField.id, response);
    }
  }, [response])

  return (
    <Segment basic>
      <Form>
        <Form.Input placeholder={props.surveyField.content.placeholder} value={response} fluid onChange={(e, {name, value}) => setResponse(value)} />
      </Form>
    </Segment>
  )
}

const FormInputCheckbox = (props) => {
  const [response, setResponse] = useState('');

  useEffect(() => {
    if(props.response === undefined || Object.keys(props.response).length == 0) {
      let newResponse = {};

      props.surveyField.content.labels.map((label, i) => {
        newResponse[label] = false;
      })

      setResponse(newResponse);
    } else {
      setResponse(props.response)
    }
  }, [props])

  useEffect(() => {
    if(props.writeResponse !== undefined) {
      let empty = true;
      Object.values(response).slice().map((value, i) => {
        if(value) {
          empty = false;
        }
      })

      if(!empty) {
        props.writeResponse(props.surveyField.id, response);
      } else {
        props.writeResponse(props.surveyField.id, '');
      }

    }
  }, [response])

  const handleResponse = (label, checked) => {
    let newResponse = { ...response };
    newResponse[label] = checked;
    setResponse(newResponse);
  }

  return (
    <Segment basic>
      <Form>
        { props.surveyField.content.labels.map((label, i) => {
            return (
              <Form.Field key={i}>
                <Checkbox checked={response[label]} label={label} onChange={(e, {label, checked}) => handleResponse(label, checked)} />
              </Form.Field>
            )
          })
        }
      </Form>
    </Segment>
  )
}

const FormInputTextarea = (props) => {
  const [response, setResponse] = useState(props.response);

  useEffect(() => {
    if(props.response !== undefined) {
      setResponse(props.response);
    } else {
      setResponse("");
    }
  }, [props])

  useEffect(() => {
    if(props.writeResponse !== undefined) {
      props.writeResponse(props.surveyField.id, response);
    }
  }, [response])

  return (
    <Segment basic>
      <Form>
        <Form.TextArea placeholder={props.surveyField.content.placeholder} value={response} onChange={(e, {name, value}) => setResponse(value)} />
      </Form>
    </Segment>
  )
}

const FormInputNames = (props) => {
  const [response, setResponse] = useState([]);
  const [teams, setTeams] = useState([]);
  const [elementNames, setElementNames] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    Api.get('programs/' + props.program_id + '/teams')
    .then((res) => {
      let newTeams = res.data;

      if(props.teams.length === 1 && props.teams[0] === -1) {
        if(props.participantId === -1) {
          newTeams = []
        } else {
          newTeams = newTeams.filter(team => {
            let find = false;
            for(let i=0; i<team.participants.length; i++) {
              if(props.participantId === team.participants[i].id) {
                find = true;
                break;
              }
            }
            return find;
          });
        }
      } else {
        if(props.surveyField.content.teams.length > 0) {
          newTeams = newTeams.filter(team => props.surveyField.content.teams.includes(team.id));
        }
      }

      setTeams(newTeams);
    })
    .catch((error) => {
      console.log(error);
      setLoading(false);
    })
  }, [props.teams])

  useEffect(() => {
    if(props.response !== undefined) {
      setResponse(props.response);
    }
  }, [props])

  useEffect(() => {
    if(props.writeResponse !== undefined) {
      props.writeResponse(props.surveyField.id, response);
    }
  }, [response])

  useEffect(() => {

  }, [teams])

  const activeParticipant = (value) => {
    const index = response.findIndex((element) => element === value);
    let newResponse = [...response];

    if(index === -1) {
      newResponse.push(value);
    } else {
      newResponse.splice(index, 1);
    }

    setResponse(newResponse);
  }

  useEffect(() => {
    if(teams.length > 0) {
      setLoading(true);
      const newElementNames = teams.map((team, i) => {
        return (
          <Segment key={i} basic>
            <Header as="h4">{team.name}</Header>
            <Segment basic>
            <Grid stackable verticalAlign="middle">
              { team.participants.map((participant, j) => {
                if(props.participantId === -1 || participant.id !== props.participantId) {
                  return <Grid.Column width={4} key={j}><Button basic={!response.includes(participant.name)} color='black' content={participant.name} circular onClick={() => activeParticipant(participant.name)} /></Grid.Column>
                }
              })}
            </Grid>
            </Segment>
          </Segment>
        )
      })

      setElementNames(newElementNames);
      setLoading(false);
    } else {
      setElementNames([]);
    }
  }, [teams, response])

  return (
    <Segment basic loading={loading}>
      <Segment basic style={{overflowY: "auto", maxHeight: "250px", padding: 0}} >
        {elementNames}
      </Segment>
    </Segment>
  )
}

const FormInputRadio = (props) => {
  const [response, setResponse] = useState('');

  useEffect(() => {
    if(props.response !== undefined) {
      setResponse(props.response);
    }
  }, [props])

  useEffect(() => {
    if(props.writeResponse !== undefined) {
      props.writeResponse(props.surveyField.id, response);
    }
  }, [response])

  return (
    <Segment basic>
      <Form>
        { props.surveyField.content.labels.map((label, i) => {
          return (
            <Form.Field key={i}>
              <Checkbox radio label={label} checked={response === label} onChange={() => setResponse(label)} />
            </Form.Field>
          )
        })
        }
      </Form>
    </Segment>
  )
}

const FormInputSlider = (props) => {
  const [response, setResponse] = useState('');
  const [marks, setMarks] = useState([]);

  useEffect(() => {
    if(props.surveyField.content.min !== "" && props.surveyField.content.max !== "") {
      let newMarks = [];
      for(let i = props.surveyField.content.min; i<=props.surveyField.content.max; i += props.surveyField.content.step) {
        newMarks.push({
          value: i,
          label: i
        });
      }
      setMarks(newMarks);
    }

    if(props.response !== undefined) {
      setResponse(props.response);
    }
  }, [props])

  useEffect(() => {
    if(props.writeResponse !== undefined) {
      props.writeResponse(props.surveyField.id, '' + response);
    }
  }, [response])

  return (
    <Segment basic>
      <Grid columns={3} stackable>
        <Grid.Column width={3} textAlign="center">
          <Header as="h4" style={{fontSize: "1em"}}>{props.surveyField.content.textMin}</Header>
        </Grid.Column>
        <Grid.Column width={10}>
        { response === "" ?
            <Slider
              key="slider-unmodified"
              style={{color: "#a3a3a0"}}
              marks={marks}
              min={props.surveyField.content.min}
              max={props.surveyField.content.max}
              step={props.surveyField.content.step}
              defaultValue={props.surveyField.content.default}
              valueLabelDisplay="auto"
              onChangeCommitted={(e, value) => setResponse(value)}
            />
          :
            <Slider
              key="slider-modified"
              style={{color: "#1976d2"}}
              marks={marks}
              min={props.surveyField.content.min}
              max={props.surveyField.content.max}
              step={props.surveyField.content.step}
              value={parseInt(response)}
              valueLabelDisplay="auto"
              onChangeCommitted={(e, value) => setResponse(value)}
            />
        }
          
        </Grid.Column>
        <Grid.Column width={3} textAlign="center">
          <Header as="h4" style={{fontSize: "1em"}}>{props.surveyField.content.textMax}</Header>
        </Grid.Column>
      </Grid>
    </Segment>
  )
}

const FormInputDate = (props) => {
  const [response, setResponse] = useState('');
  const [open, setOpen] = useState(false);
  const [maxDate, setMaxDate] = useState(props.surveyField.content.maxDate);
  const [minDate, setMinDate] = useState(props.surveyField.content.minDate);
  const [viewDate, setViewDate] = useState([]);
  const [formatDate, setFormatDate] = useState("yyyy-MM-DD");

  useEffect(() => {
    if(props.response !== undefined) {
      setResponse(props.response);
    }
  }, [props])

  useEffect(() => {
    if(props.writeResponse !== undefined) {
      let newResponse;

      if(typeof response === "object" && "_isValid" in response && response._isValid) { // is a correct date (object)
        newResponse = response.format('yyyy-MM-DD');
        props.writeResponse(props.surveyField.id, newResponse);
      }
    }
  }, [response])

  useEffect(() => {
    switch(props.surveyField.content.typeDate) {
      case 0:
        setViewDate(["year", "month", "day"]);
        setFormatDate("yyyy-MM-DD");
        break;
      case 1:
        setViewDate(["month", "day"]);
        setFormatDate("MM-DD");
        break;
      case 2:
        setViewDate(["day"]);
        setFormatDate("DD");
        break;
      case 3:
        setViewDate(["month"]);
        setFormatDate("MM");
        break;
      case 4:
        setViewDate(["year"]);
        setFormatDate("yyyy");
        break;
    }
  }, [props.surveyField.content.typeDate])

  useEffect(() => {
    if(props.surveyField.content.maxDate !== null) {
      setMaxDate(new Date(props.surveyField.content.maxDate));
    }
  }, [props.surveyField.content.maxDate])

  useEffect(() => {
    if(props.surveyField.content.minDate !== null) {
      setMinDate(new Date(props.surveyField.content.minDate));
    }
  }, [props.surveyField.content.minDate])

  return (
    <Segment basic>
      <LocalizationProvider dateAdapter={AdapterMoment}>
        <MobileDatePicker
          label="Enter date"
          showToolbar={false}
          views={viewDate}
          onClose={() => setOpen(false)}
          value={response || null}
          inputFormat={formatDate}
          minDate={minDate}
          maxDate={maxDate}
          onChange={(value) => setResponse(value)}
          renderInput={(params) => <TextField {...params} />}
          componentsProps={{
            actionBar: {
              actions: ['accept', 'clear'],
            },
          }}
          />
      </LocalizationProvider>
    </Segment>
  )
}

const FormInputMatrix = (props) => {
  const [listResponse, setListResponse] = useState({})
  const [elementInput, setElementInput] = useState([]);

  useEffect(() => {
    let newListResponse = {}
    for(let i=0; i<props.surveyField.content.contents.length; i++) {
      newListResponse[props.surveyField.content.contents[i].subtitle] = "";
    }

    if(props.response !== undefined && Object.keys(props.response).length > 0) {
      newListResponse = JSON.parse(JSON.stringify(props.response))
    }

    let newElementInput = [];
    if(props.surveyField.content.contents.length > 0) {
      for(let i=0; i<props.surveyField.content.contents.length; i++) {
        let content = props.surveyField.content.contents[i];
        let subtitle = props.surveyField.content.contents[i].subtitle;
        let listSubtitle = Object.keys(newListResponse)

        newElementInput.push(
          <Header as='h4' key={"subtitle_" + i} className="display-linebreak">
            {content.subtitle}
          </Header>
        )
        
        switch(content.category) {
          case "inputField":
            newElementInput.push(
              <FormInputField key={"input_" + i} surveyField={content} response={newListResponse[listSubtitle[i]]} writeResponse={(id, content) => {setResponseMatrix(subtitle, content)}} />
            )
            break;
          case "inputCheckbox":
            newElementInput.push(
              <FormInputCheckbox key={"input_" + i} surveyField={content} response={newListResponse[listSubtitle[i]]} writeResponse={(id, content) => {setResponseMatrix(subtitle, content)}} />
            )
            break;
          case "inputRadio":
            newElementInput.push(
              <FormInputRadio key={"input_" + i} surveyField={content} response={newListResponse[listSubtitle[i]]} writeResponse={(id, content) => {setResponseMatrix(subtitle, content)}} />
            )
            break;
          case "inputTextarea":
            newElementInput.push(
              <FormInputTextarea key={"input_" + i} surveyField={content} response={newListResponse[listSubtitle[i]]} writeResponse={(id, content) => {setResponseMatrix(subtitle, content)}} />
            )
            break;
          case "inputSlider":
            newElementInput.push(
              <FormInputSlider key={"input_" + i} surveyField={content} response={newListResponse[listSubtitle[i]]} writeResponse={(id, content) => {setResponseMatrix(subtitle, content)}} />
            )
            break;
          case "inputDate":
            newElementInput.push(
              <FormInputDate key={"input_" + i} surveyField={content} response={newListResponse[listSubtitle[i]]} writeResponse={(id, content) => {setResponseMatrix(subtitle, content)}} />
            )
            break;
        }
      }
    }

    setListResponse(newListResponse);
    setElementInput(newElementInput)

  }, [props])

  useEffect(() => {
    if(props.writeResponse !== undefined) {
      props.writeResponse(props.surveyField.id, listResponse);
    }
  }, [listResponse])

  const setResponseMatrix = (subtitle, newResponse) => {
    setListResponse((oldState) => ({...oldState, [subtitle]: newResponse }));
  }

  return (
    <Segment basic>
      {elementInput}
    </Segment>
  )
}

const FormInputTopic = (props) => {
  const [listStyle, setListStyle] = useState([])
  
  useEffect(() => {
    let newListStyle = [];

    props.surveyField.content.paragraphs.map((paragraph, i) => {
      let style = {} //{ "fontSize": "1.3em" }, "textIndent": "1em"}
      if(paragraph.italic) {
        style["fontStyle"] = "italic";
      }

      if(paragraph.bold) {
        style["fontWeight"] = 560;
      }

      newListStyle.push(style);
    })

    setListStyle(newListStyle);
  }, [props])
  
  return (
    <Segment basic>
      <Form>
        { props.surveyField.content.paragraphs.map((paragraph, i) => {
          return (
            <p key={i} className="display-linebreak" style={listStyle[i]}>{ paragraph.text }</p>
          )
        })
        }
      </Form>
    </Segment>
  )
}

export { 
  FormIntro, 
  FormInput, 
  FormWrapDependency, 
  FormInputField, 
  FormInputCheckbox, 
  FormInputRadio, 
  FormInputTextarea, 
  FormInputNames, 
  FormInputSlider, 
  FormInputDate, 
  FormInputMatrix,
  FormInputTopic, 
}

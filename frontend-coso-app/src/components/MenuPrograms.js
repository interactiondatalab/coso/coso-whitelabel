import React, { useEffect, useState, useContext } from 'react';
import { useLocation } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { Segment,
  Menu,
  Container,
  Dropdown,
  Grid
} from 'semantic-ui-react';

import { Context } from '../utils/Context';

const MenuPrograms = () => {
  const context = useContext(Context);
  const location = useLocation();
  const [programItems, setProgramItems] = useState("");

  useEffect(() => {
    if(!context.firstLoad) {
      context.loadPrograms(context);
    }
  }, [context.firstLoad]);

  const selectProgram = async(program, indexProgram) => {
    context.setCurrentProgram([program, indexProgram]);
  }

  const MenuProgramItems = () => {
    let listPermissionProgram = {"Participant": [], "God": [], "Moderator": [], "Admin": []};
    Object.keys(context.programs).map((program) => {
      context.programs[program].map((p, indexProgram) => {
        switch(p.permission) {
          case 0:
            listPermissionProgram["Participant"].push([program, indexProgram]);
            break;
          case 1:
            listPermissionProgram["Moderator"].push([program, indexProgram]);
            break;
          case 2:
            listPermissionProgram["Admin"].push([program, indexProgram]);
            break;
          case 3:
            listPermissionProgram["God"].push([program, indexProgram]);
            break;
          default:
            break;
        }
      })
    })

    let menuProgramItems = [];
    let i = 0;
    ["God", "Admin", "Moderator", "Participant"].map((role) => {
      if(listPermissionProgram[role].length > 0) {
        menuProgramItems.push(
          <Dropdown.Item active disabled color="black" key={menuProgramItems.length}><strong>{role}</strong></Dropdown.Item>
        )

        if(role === "God") {
          menuProgramItems.push(
            <Dropdown.Item as={Link} to={"/program/create"} key={menuProgramItems.length}><i>Create a program</i></Dropdown.Item>
          )
          menuProgramItems.push(
            <Dropdown.Divider key={menuProgramItems.length} />
          )
        }

        listPermissionProgram[role].map((programArray) => {
          menuProgramItems.push(
            <Dropdown.Item onClick={() => { selectProgram(programArray[0], programArray[1]) }} as={Link} to={"/program/" + programArray[0] + "/dataviz"} key={menuProgramItems.length}>{context.programs[programArray[0]][programArray[1]].name}</Dropdown.Item>
          )

        })

        menuProgramItems.push(
          <Dropdown.Divider key={menuProgramItems.length} />
        )
      }
    })






    return menuProgramItems;
  }

  useEffect(() => {
    setProgramItems(
      <Grid.Column width={10}>
        <Dropdown text={context.currentProgram.length === 2 ? context.programs[context.currentProgram[0]][context.currentProgram[1]].name : "Select a program"} pointing className='link item'>
          <Dropdown.Menu>
            <MenuProgramItems />
          </Dropdown.Menu>
        </Dropdown>
      </Grid.Column>
    );
  }, [context.programs, context.currentProgram])

  if(!context.firstLoad) {
    return (
      <Segment
      inverted
      textAlign='center'
      style={{ padding: '1em 0em' }}
      vertical>
        <Menu
          inverted
          secondary
          size='large'
          attached='top'
          tabular
        >
          <Container>
            <Grid columns={2} stackable>
              { programItems }
              <Grid.Column width={6}>
                { context.currentProgram.length === 2 ?
                    <Menu inverted pointing secondary>
                      { context.programs[context.currentProgram[0]][context.currentProgram[1]].permission > 0 ?
                          <>
                          <Menu.Item active={location.pathname === "/program/" + context.currentProgram[0] + "/dataviz"} as={Link} to={"/program/" + context.currentProgram[0] + "/dataviz"}>Dashboard</Menu.Item>
                          <Menu.Item active={location.pathname === "/program/" + context.currentProgram[0] + "/surveys"} as={Link} to={"/program/" + context.currentProgram[0] + "/surveys"}>Surveys</Menu.Item>
                          <Menu.Item active={location.pathname === "/program/" + context.currentProgram[0]} as={Link} to={"/program/" + context.currentProgram[0]}>Settings</Menu.Item>
                          </>
                        :
                          <>
                          <Menu.Item></Menu.Item>
                          <Menu.Item active={location.pathname === "/program/" + context.currentProgram[0] + "/surveys"} as={Link} to={"/program/" + context.currentProgram[0] + "/surveys"}>Surveys</Menu.Item>
                          </>
                      }
                    </Menu>
                  :
                    null
                }
              </Grid.Column>
            </Grid>
          </Container>
        </Menu>
      </Segment>
    )
  }
}

export default MenuPrograms;

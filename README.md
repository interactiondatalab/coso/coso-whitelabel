# README

## Installation

1) Set up the environment variables:
Make sure to modify or generate new secrets before serving your application to production (especially SECRET_BASE and DEVISE_JWT_SECRET_KEY).
Then run:
```
cp env.sample .env
```
2) Create directory for database:
```
mkdir storage/db
```

3) Build and run the application:
```
docker-compose build
docker-compose up
```

Please, refer to backend-coso-app/README.md to continue installation.